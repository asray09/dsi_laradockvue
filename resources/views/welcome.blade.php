<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>

        <!-- Fonts -->
        
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
        @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap');
                * {
                    margin: 0;
                    padding: 0;
                } 
            .loader-wrapper {
                position: absolute;
                width: 100%;
                height: 100%;
                background: #A21920;
                transition: 0.5s ease-in-out;
            }
            .loader {
                position: absolute;
                top: 50%;
                left: 50%;
                background: #fff;
                width: 50px;
                height: 50px;
                border-radius: 50%;
                transform: translate(-50%, -50%);
                animation: bounce 1s ease-in-out infinite;
                transition: 0.5s ease-in-out;
            }
            @keyframes bounce {
                0% {
                    top: 15%;
                }
                50% {
                    top: 50%;
                }
                100% {
                    top: 15%;
                }
            }
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                padding: 0;
                position: relative;
                justify-content: center;
	            align-items: center;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 54px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
           
        </style>
    </head>
    <body>
    

            <div class="flex-center position-ref full-height">
             @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="loader-wrapper">
			<div class="loader"></div>
		</div>
        <div class="content">

                <div class="title m-b-md">
                Programming Test
                </div>
                <img src="{{ ('images/era.png') }}" >
                <div>
                Digital Creator. Asep Rayana
                </div>
            </div>
        </div>

        <script>
        const loaderWrapper = document.querySelector('.loader-wrapper');
        const loader = document.querySelector('.loader');

                const loadingTime = 3500;
        setTimeout(() => {
	        loader.style.transform = 'scale(100)';
            }, loadingTime);

        setTimeout(() => {
	        loaderWrapper.style.display = 'none';
        }, loadingTime + 150);
        </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>
    </body>
</html>
