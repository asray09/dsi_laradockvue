<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('version', function () {
    return response()->json(['version' => config('app.version')]);
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    Log::debug('User:' . serialize($request->user()));
    return $request->user();
});
Route::get('profile', 'API\V1\ProfileController@profile');
Route::put('profile', 'API\V1\ProfileController@updateProfile');

Route::post('change-password', 'API\V1\ProfileController@changePassword');
Route::get('group/list', 'API\V1\GroupController@list');
Route::post('upload','UploadController@upload');

Route::apiResources([
    'user' => 'API\V1\UserController',
    'group' => 'API\V1\GroupController',

]);
