<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\Users\UserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\Gate::allows('isAdmin')) {
            return $this->unauthorizedResponse();
        }
        // $this->authorize('isAdmin');

        $users = User::latest()->with('group')->paginate(10);
         return $this->sendResponse($users, 'Users list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Users\UserRequest  $request
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(UserRequest $request)
    {
        if($request->photo){

            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('images/upload/').$name);
            $request->merge(['photo' => $name]);
           
        }
        $user = User::create([
            'name' => $request['name'],
            'type' => $request['type'],
            'gender' => $request['gender'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'remarks' => $request['remarks'],
            'group_id' => $request->get('group_id'),            
            'photo' => $request['photo'],
        ]);
       

        return $this->sendResponse($user, 'User Created Successfully');
    }
    public function show($id)
    {
        $user = $this->user->with(['group'])->findOrFail($id);
        return $this->sendResponse($user, 'User Details');
    }
 
    /**
     * Update the resource in storage
     *
     * @param  \App\Http\Requests\Users\UserRequest  $request
     * @param $ic
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);

        $currentPhoto = $user->photo;

        if($request->photo != $currentPhoto){

            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            \Image::make($request->photo)->save(public_path('images/upload/').$name);
            $request->merge(['photo' => $name]);

            $userPhoto = public_path('images/upload/').$currentPhoto;

            if(file_exists($userPhoto)){

                @unlink($userPhoto);
                
            }
           
        }

        $user->update($request->all());
     
        return $this->sendResponse($user, 'User Information has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $user = User::findOrFail($id);
        // delete the user

        $user->delete();

        return $this->sendResponse([$user], 'User has been Deleted');
    }
    public function upload(Request $request)
    {
        $photo = time() . '.' . $request->file->getClientOriginalExtension();
        $request->file->move(public_path('upload'), $photo);
        return response()->json(['success' => true]);
    }

}


