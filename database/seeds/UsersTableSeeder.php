<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->where('email', 'asep.rayana09@gmail.com')->delete();

        DB::table('users')->insert([
            'name' => 'Asep rayana',
            'gender' => 'male',
            'email' => 'asep.rayana09@gmail.com',
            'password' => bcrypt('12345678'),
            'type' => 'admin',
            'remarks' => 'administrator',
            'created_at' => '2020-04-17 01:00:00',
            'group_id' => 1,
            'photo' => 'images/profile.png',

        ]);
    }
}
