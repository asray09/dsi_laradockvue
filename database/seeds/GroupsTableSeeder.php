<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->truncate();

        DB::table('groups')->insert(
            [
                [
                    'name' => 'Programmer',
                    'remarks' => 'Im a programmer',
                    'created_at' => '2020-04-19 07:20:03',
                    //'description' => Str::words(50),
                ],
                [
                    'name' => 'Design',
                    'remarks' => 'Im a Im a web designer',
                    'created_at' => '2020-04-19 07:20:03',

                ],
               
            ]
        );
    }
}
